use crate::tokens::fungible_token::Token;

/// Represents a unique "tokenized" item
pub trait NonFungibleToken : Token {

    /// get the unique name of the token
    fn get_name(&self, account_id: &'static str) -> &'static str {Self::get_title()}

    /// get the url/ipfs link of the unique image for this nft
    fn get_image_link(&self, account_id: &'static str) -> &'static str {Self::get_icon_link()}

    /// get the edition out of the number minted of this nft
    fn get_edition(&self, account_id: &'static str) -> i32;

    /// get the account id of the owner of this NFT
    fn get_owner(&self) -> &'static str;
}