/*use std::fmt;
use std::error::Error;

/// Error for insufficient balance when attempting to do something with a token
#[derive(Debug)]
pub struct InsufficientBalanceError{ operation_name: String}
impl fmt::Display for InsufficientBalanceError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f,"TX FAILURE: insufficient balance to execute {}",self.operation_name)
    }
}
impl Error for InsufficientBalanceError {
    fn description(&self) -> &str {
        "TX FAILURE: insufficient balance to execute" + &self.operation_name
    }
}*/

/// Represents a "tokenized" item
pub trait Token {

    /// get the name of the token
    fn get_title() -> &'static str;

    /// get the url/ipfs link of the icon
    fn get_icon_link() -> &'static str;

    /// get the total supply or editions of this token circulating
    fn get_total_circulation() -> i32;

    /// the unique id of the typ eof this token, like it's SC id.
    fn get_type_id() ->  &'static str;

    /// get the unique id of this token
    fn get_uid(&self) ->  &'static str {Self::get_type_id()}

    /// if this individual token is fungible
    fn is_fungible(&self) -> bool {self::get_uid() == Self::get_type_id()}

    /// get the balance of this type of token for a given user id.
    fn get_balance_for(account_id: &'static str) -> Vec<dyn Token>;

    /// try to exchange the given amount of tokens of the specified types between the two given accounts.
    /*fn exchange_between(from_account: &'static str, to_account: &'static str, quantity: i32, tokens: Self<dyn Token>)
        -> Result<(), InsufficientBalanceError>;*/
}