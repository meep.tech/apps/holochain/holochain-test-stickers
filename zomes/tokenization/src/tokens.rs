pub mod fungible_token;
pub mod non_fungible_token;

pub use crate::fungible_token::Token;
pub use crate::non_fungible_token::Token;