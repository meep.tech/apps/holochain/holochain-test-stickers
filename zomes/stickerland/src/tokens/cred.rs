use hdk::prelude::*;
use tokenization::tokens::fungible_token::Token;

#[hdk_entry(id = "Cred")]
pub struct Cred;

impl Token for Cred {
    fn get_title() -> &'static str {"Cred"}

    fn get_icon_link() -> &'static str {""}

    fn get_total_circulation() -> i32 {1_000_000}

    fn get_type_id() ->  &'static str {"CRED UUID"}

    fn is_fungible(&self) -> bool {true}

    fn get_balance_for(account_id: &'static str) -> Vec<dyn Token> {
        todo!()
    }
}